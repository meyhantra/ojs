<?php return array (
  'submission.submit.title' => 'Serahkan Artikel',
  'submission.submit.userGroup' => 'Serahkan dalam peran saya sebagai...',
  'submission.submit.userGroupDescription' => 'Pilih peran yang Anda gunakan dalam penyerahan naskah ini.',
  'submission.upload.selectComponent' => 'Pilih komponen artikel',
  'submission.title' => 'Judul Artikel',
  'submission.title.tip' => 'Jenis naskah biasanya berupa \'gambar\', \'teks\', atau jenis multimedia lain termasuk \'perangkat lunak\' atau \'interaktif\'.  Mohon pilih salah satu yang paling sesuai dengan apa yang Anda serahkan.  Contoh dapat dilihat di <a target="_blank" href="http://dublincore.org/documents/2001/04/12/usageguide/generic.shtml#type">http://dublincore.org/documents/2001/04/12/usageguide/generic.shtml#type</a>',
  'submission.submit.newSubmissionMultiple' => 'Mulai penyerahan Naskah Baru di',
  'submission.submit.newSubmissionSingle' => 'Naskah Baru',
  'grid.action.workflow' => 'Alur Penyerahan Naskah',
  'submission.issueEntry' => 'Metadata',
  'submission.submit.whatNext.description' => 'Pengelola jurnal telah diberitahu tentang naskah yang Anda serahkan, dan sebuah surel telah dikirimkan ke alamat Anda sebagai konfirmasi. Saat editor meninjau penyerahan naskah tersebut, Anda akan dihubungi.',
  'grid.action.galleyInIssueEntry' => 'Galai tampil di entri terbitan',
  'grid.action.issueEntry' => 'Lihat metadata naskah ini',
  'submission.publication' => 'Publikasi',
  'submission.event.articleGalleyMadeAvailable' => 'Galai artikel "{$galleyFormatName}" tersedia.',
  'submission.event.articleGalleyMadeUnavailable' => 'Galai artikel "{$galleyFormatName}" tidak tersedia.',
  'submission.event.publicIdentifiers' => 'Identitas publik dari naskah ini telah diperbarui.',
  'grid.issueEntry.availableGalley.title' => 'Persetujuan Format',
  'grid.issueEntry.availableGalley.removeMessage' => '<p>Galai ini <em>tidak tersedia lagi</em> untuk pembaca.</p>',
  'grid.issueEntry.availableGalley.message' => '<p>Galai ini <em>akan tersedia</em> untuk pembaca.</p>',
  'grid.action.availableArticleGalley' => 'Sediakan galai ini',
  'submission.galleyFiles' => 'Berkas Galai',
  'submission.proofReadingDescription' => 'Editor tata letak mengunggah file siap terbit.  Gunakan <em>Tugaskan Auditor</em> untuk menentukan penulis atau pengguna lainnya untuk melakukan proofreading terhadap galai, dan mengunggah file yang telah dikoreksi sebelum publikasi dilakukan.',
  'grid.action.approveProof' => 'Setujui proof ini untuk dimasukkan ke dalam galai.',
  'grid.action.pageProofApproved' => 'Proof ini telah disetujui untuk dimasukkan ke dalam galai.',
  'submission.submit.titleAndSummary' => 'Judul dan Abstrak',
  'submission.submit.upload.description' => 'Unggah berkas yang terkait dengan naskah ini, termasuk artikel, multimedia, set data, artwork, dll.',
  'submission.pageProofs' => 'Mengoreksi',
  'submission.confirmSubmit' => 'Anda yakin akan menyerahkan naskah ke jurnal ini?',
  'workflow.review.externalReview' => 'Ulasan',
  'editor.submission.decision.sendExternalReview' => 'Kirim untuk Ulasan',
  'submission.event.issueMetadataUpdated' => 'Metadata naskah yang diserahkan telah diperbaharui.',
  'submission.upload.fileContents' => 'Komponen Artikel',
  'submission.complete' => 'Disetujui',
  'submission.incomplete' => 'Menunggu Persetujuan',
  'submission.submit.form.localeRequired' => 'Silakan pilih bahasa yang hendak digunakan.',
  'submission.submit.submissionChecklist' => 'Daftar Tilik Penyerahan Naskah',
  'submission.submit.submissionChecklistDescription' => 'Sebelum melanjutkan, Anda harus membaca dan menyatakan bahwa Anda telah memenuhi persyaratan berikut ini.',
  'submission.submit.privacyStatement' => 'Pernyataan Privasi',
  'submission.submit.contributorRole' => 'Peran Kontributor',
  'submission.submit.form.authorRequired' => 'Dibutuhkan setidaknya satu penulis.',
  'submission.submit.form.authorRequiredFields' => 'Nama depan, nama belakang, dan surel setiap penulis harus ada.',
  'submission.submit.form.titleRequired' => 'Mohon tuliskan judul naskah Anda.',
  'submission.submit.form.abstractRequired' => 'Mohon masukkan abstrak naskah Anda.',
  'submission.submit.form.contributorRoleRequired' => 'Silakan pilih peran kontributor.',
  'submission.citationFormat.notFound' => 'Format sitasi yang diminta tidak ditemukan.',
  'submission.metadataDescription' => 'Spesifikasi ini dibuat berdasarkan set metadata Dublin Core, suatu standar internasional untuk mendeskripsikan isi jurnal.',
  'section.any' => 'Bagian Mana Saja',
  'publication.publish.confirmation.futureIssue' => 'Seluruh prasyarat penerbitan telah terpenuhi. Ini akan diterbitkan saat {$issue} diterbitkan. Apakah Anda yakin ingin menjadwalkan ini untuk diterbitkan?',
  'publication.publish.confirmation' => 'Seluruh prasyarat penerbitan telah terpenuhi. Anda yakin ingin menerbitkan ini?',
  'publication.issue.success' => 'Rincian terbitan untuk penerbitan ini telah diperbarui.',
  'publication.invalidIssue' => 'Terbitan untuk penerbitan ini tidak dapat ditemukan.',
  'publication.invalidSection' => 'Bagian untuk penerbitan ini tidak dapat ditemukan.',
  'publication.datePublished.description' => 'Tanggal publikasi telah diset secara otomatis saat terbitan ini diterbitkan. Jangan memasukkan tanggal publikasi kecuali artikel tersebut telah diterbitkan di tempat lain dan Anda perlu memajukan tanggalnya.',
  'galley.publicationNotFound' => 'Publikasi untuk galai ini tidak dapat ditemukan.',
  'galley.editPublishedDisabled' => 'Galai dari publikasi ini tidak dapat disunting karena telah diterbitkan.',
  'catalog.category.heading' => 'Semua butir',
  'catalog.browseTitles' => '{$numTitles} butir',
  'submission.sectionOptions' => 'Pilih Bagian',
  'submission.submit.form.wordCountAlert' => 'Abstrak Anda terlalu panjang. Mohon dipersingkat menurut batasan kata yang ditunjukkan untuk bagian ini.',
  'submission.abstract.wordCount.description' => 'Abstrak harus berjumlah {$wordCount} kata atau kurang.',
  'publication.scheduledIn' => 'Dijadwalkan untuk terbit di <a href="{$issueUrl}">{$issueName}</a>.',
  'publication.publishedIn' => 'Diterbitkan di <a href="{$issueUrl}">{$issueName}</a>.',
  'publication.required.issue' => 'Publikasi ini harus dimasukkan dalam sebuah nomor terbitan sebelum dapat dipublikasikan.',
  'publication.publish.confirmation.backIssue' => 'Semua persyaratan publikasi telah terpenuhi.  Naskah ini akan segera diterbitkan dalam {$issue}. Anda yakin ingin menerbitkan?',
  'submission.submit.noContext' => 'Jurnal untuk naskah yang diserahkan tidak dapat ditemukan.',
);