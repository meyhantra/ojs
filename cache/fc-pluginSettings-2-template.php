<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<div id="sidebarDownloadTemplate" class="block"><span class="blockTitle">Journal Template</span>
<p>Untuk penulis yang ingin mengirimkan artikel, harap gunakan <em>template</em> standar kami yang bisa di-<em>download</em> dari link di bawah ini:</p>
<a id="downloadTemplatePenulis" class="blockTitle" href="/public/files/jelc/template_author.doc"><img style="width: 75px;" src="/plugins/blocks/downloadTemplate/icon-download.png" alt=""></a> <br><br>
<p>Untuk pengelola jurnal harap gunakan template standar berikut.</p>
<a id="downloadTemplatePengelola" class="blockTitle" href="/plugins/blocks/downloadTemplate/template_journal-pengelola.doc"><img style="width: 75px;" src="/plugins/blocks/downloadTemplate/icon-download.png" alt=""></a></div>',
    'id_ID' => '',
  ),
  'enabled' => true,
);