<?php return array (
  'blockContent' => 
  array (
    'en_US' => '<p><span class="blockTitle">Journal Template</span></p>
<p>Untuk penulis yang ingin mengirimkan artikel, harap gunakan <em>template</em> standar kami yang bisa di-<em>download</em> dari link di bawah ini:</p>
<p><a id="downloadTemplatePenulis" class="blockTitle" href="/public/files/jelc/template_author.doc"><img style="width: 75px;" src="/plugins/blocks/downloadTemplate/icon-download.png" alt=""></a> <br><br></p>
<p>Untuk pengelola jurnal harap gunakan template standar berikut.</p>
<p>&nbsp;</p>',
  ),
  'enabled' => true,
);