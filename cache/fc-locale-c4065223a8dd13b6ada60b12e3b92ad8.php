<?php return array (
  'plugins.importexport.medra.description' => 'Ekspor metadata issue, artikel, galley di Onix untuk format DOI (O4DOI) dan daftarkan DOI pada agen pendaftaran mEDRA .',
  'plugins.importexport.medra.displayName' => 'Plugin Ekspor/Registrasi mEDRA',
  'plugins.importexport.medra.intro' => '
		Jika ingin mendaftar DOI di mEDRA, ikuti petunjuk pada
		<a href="http://www.medra.org/en/guide.htm" target="_blank">halaman beranda mEDRA</a>
		untuk mendapatkan username dan password. Bila tidak punya akun, 
		kamu tetap bisa mengekspor ke dalam format XML mEDRA (Onix untuk DOI)
		tapi kamu tidak bisa meregister DOI di mEDRA melalui aplikasi OJS. 
		Perlu diketahui bahwa password akan disimpan sebagai teks biasa, misalnya, tidak dienkripsi, karena
		persyaratan layanan registrasi mEDRA.
	',
  'plugins.importexport.medra.cliUsage' => 'Penggunaan: 
{$scriptName} {$pluginName} mengkespor [xmlFileName] [journal_path] {issues|articles|galleys} objectId1 [objectId2] ...
{$scriptName} {$pluginName} meregister [journal_path] {issues|articles|galleys} objectId1 [objectId2] ...
',
  'plugins.importexport.medra.senderTask.name' => 'Tugas registrasi otomatis mEDRA',
  'plugins.importexport.medra.workOrProduct' => 'Catatan: DOI yang didaftarkan untuk artikel akan diekspor ke mEDRA sebagai <a href="http://www.medra.org/en/metadata_td.htm" target="_blank">\'karya\'</a>. DOI yang diberikan untuk galley akan diekspor sebagai <a href="http://www.medra.org/en/metadata_td.htm" target="_blank">\'manifestasi\'</a>.',
  'plugins.importexport.medra.settings.form.testMode.description' => 'Gunakan API test mEDRA (lingkungan uji coba) untuk registrasi DOI. Jangan lupa menghapus pilihan ini untuk tahap produksi.',
  'plugins.importexport.medra.settings.form.automaticRegistration.description' => 'OJS akan mendaftarkan DOI secara otomatis ke mEDRA. Perlu diketahui bahwa proses ini memakan waktu setelah publikasi diproses (misalnya, bergantung pada cronjob Anda). Kamu bisa  periksa semua DOI yang belum diregister.',
  'plugins.importexport.medra.settings.form.exportIssuesAs.label' => 'Ekspor issue sebagai',
  'plugins.importexport.medra.settings.form.manifestation' => 'manifestasi',
  'plugins.importexport.medra.settings.form.work' => 'Karya',
  'plugins.importexport.medra.settings.form.exportIssuesAs' => 'Tentukan Anda ingin mengekspor issue sebagai <a href="http://www.medra.org/en/metadata_td.htm" target="_blank">\'karya\' atau \'manifestasi\'</a>.',
  'plugins.importexport.medra.settings.form.publicationCountry' => 'Pilih negara yang akan dihubungi sebagai \'negara penerbit\' ke mEDRA.',
  'plugins.importexport.medra.settings.form.fromEmailRequired' => 'Masukkan surel narahubung yang benar.',
  'plugins.importexport.medra.settings.form.fromEmail' => 'Surel',
  'plugins.importexport.medra.settings.form.fromNameRequired' => 'Masukkan narahubung teknis.',
  'plugins.importexport.medra.settings.form.fromName' => 'Narahubung',
  'plugins.importexport.medra.settings.form.fromCompanyRequired' => 'Masukkan institusi yang bertugas untuk mengekspor DOI (misalnya, institusi yang meng-hosting web server Anda).',
  'plugins.importexport.medra.settings.form.fromCompany' => 'Institusi',
  'plugins.importexport.medra.settings.form.fromFields' => 'Pihak yang akan dihubungi oleh mEDRA jika ada pertanyaan teknis:',
  'plugins.importexport.medra.settings.form.registrantNameRequired' => 'Masukkan nama institusi yang didaftarkan ke mEDRA.',
  'plugins.importexport.medra.settings.form.registrantName' => 'Nama institusi yang didaftarkan ke mEDRA',
  'plugins.importexport.medra.settings.form.usernameRequired' => 'Masukkan username mEDRA Anda. Username tidak boleh mengandung titik dua (:).',
  'plugins.importexport.medra.settings.form.username' => 'Username',
  'plugins.importexport.medra.settings.form.description' => 'Aturlah plugin ekspor mEDRA:',
);